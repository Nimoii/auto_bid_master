<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190219092259 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE search_log (id INT AUTO_INCREMENT NOT NULL, vehicle_type VARCHAR(255) NOT NULL, make_abbr VARCHAR(255) NOT NULL, results_found INT NOT NULL, request_time DATETIME NOT NULL, ip_address VARCHAR(255) NOT NULL, user_agent LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicle_type (code VARCHAR(10) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(code)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE make (code VARCHAR(10) NOT NULL, type VARCHAR(10) NOT NULL, description VARCHAR(100) NOT NULL, INDEX IDX_1ACC766E8CDE5729 (type), UNIQUE INDEX make_unique (code, description, type), PRIMARY KEY(code, type)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE model (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(10) DEFAULT NULL, make_code VARCHAR(10) DEFAULT NULL, code VARCHAR(100) NOT NULL, description VARCHAR(255) NOT NULL, INDEX IDX_D79572D98CDE5729 (type), INDEX IDX_D79572D96AFEE310 (make_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE make ADD CONSTRAINT FK_1ACC766E8CDE5729 FOREIGN KEY (type) REFERENCES vehicle_type (code)');
        $this->addSql('ALTER TABLE model ADD CONSTRAINT FK_D79572D98CDE5729 FOREIGN KEY (type) REFERENCES vehicle_type (code)');
        $this->addSql('ALTER TABLE model ADD CONSTRAINT FK_D79572D96AFEE310 FOREIGN KEY (make_code) REFERENCES make (code)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE make DROP FOREIGN KEY FK_1ACC766E8CDE5729');
        $this->addSql('ALTER TABLE model DROP FOREIGN KEY FK_D79572D98CDE5729');
        $this->addSql('ALTER TABLE model DROP FOREIGN KEY FK_D79572D96AFEE310');
        $this->addSql('DROP TABLE search_log');
        $this->addSql('DROP TABLE vehicle_type');
        $this->addSql('DROP TABLE make');
        $this->addSql('DROP TABLE model');
    }
}
