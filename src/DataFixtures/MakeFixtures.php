<?php

namespace App\DataFixtures;

use App\Entity\Make;
use App\DataFixtures\AppFixtures;
use App\DataFixtures\VehicleTypeFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class MakeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $makes = AppFixtures::loadJSON('makes.json');
        $keys = [];

        foreach($makes as $make) {
            $key = $make->code.$make->type;
            if (in_array($key, $keys)) {
                //var_dump('make key already exists: '.$key);
                continue;
            } else {
                $keys[] = $key;
            }
            $newMake = new Make();
            //$newMake->setUid($make->code.$make->description.$make->type);
            $newMake->setCode($make->code);
            $newMake->setDescription($make->description);
            
            $type = (!empty($this->hasReference('type-'.$make->type)))
                ? $this->getReference('type-'.$make->type)
                : false;
            if ($type) {
                $newMake->setType($type);
            } else {
                //var_dump($newMake, $make); die();
                continue;
            }

            $manager->persist($newMake);

            $this->setReference('maketype-'.$make->code.$make->type, $newMake);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            VehicleTypeFixtures::class,
        ];
    }
}
