<?php

namespace App\DataFixtures;

use App\Entity\Model;
use App\DataFixtures\AppFixtures;
use App\DataFixtures\VehicleTypeFixtures;
use App\DataFixtures\MakeFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ModelFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $models = AppFixtures::loadJSON('models.json');

        foreach($models as $model) {
            $newModel = new Model();
            $newModel->setCode($model->code);
            $newModel->setDescription($model->description);
            
            $type = (!empty($this->hasReference('type-'.$model->type)))
                ? $this->getReference('type-'.$model->type)
                : false;
            if ($type) {
                $newModel->setType($type);
            } else {
                //var_dump('No type for model: ',$model);
                continue;
            }

            $make = (!empty($this->hasReference('maketype-'.$model->group.$model->type)))
                ? $this->getReference('maketype-'.$model->group.$model->type)
                : false;
            if ($make) {
                $newModel->setMakeCode($make);
            } else {
                //var_dump('No make for model: ',$model);
                continue;
            }

            $manager->persist($newModel);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            VehicleTypeFixtures::class,
            MakeFixtures::class,
        ];
    }
}
