<?php

namespace App\DataFixtures;

use App\Entity\VehicleType;
use App\DataFixtures\AppFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class VehicleTypeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $vehicleTypesData = AppFixtures::loadJSON('vehicle_types.json');

        foreach($vehicleTypesData as $type) {
            $vehicleType = new VehicleType();
            $vehicleType->setCode($type->code);
            $vehicleType->setDescription($type->description);

            $manager->persist($vehicleType);

            $this->addReference('type-'.$type->code, $vehicleType);
        }

        $manager->flush();
    }
}
