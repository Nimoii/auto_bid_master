<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Config\FileLocator;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Is this only here to share loadJSON()?
        // Surely there is a much better way to do that.
    }

    public function loadJSON($filename)
    {
        $fileLocator = new FileLocator([__DIR__.'/data']);
        $path = $fileLocator->locate($filename, null, false);
        return json_decode(file_get_contents(current($path), true));
    }
}
