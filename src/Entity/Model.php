<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ModelRepository")
 */
class Model
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\VehicleType", inversedBy="models")
     * @ORM\JoinColumn(name="type", referencedColumnName="code")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Make", inversedBy="models")
     * 
     * @ORM\JoinColumns(
     *  @ORM\JoinColumn(name="make_code", referencedColumnName="code"),
     * )
     */
    private $make_code;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getType(): ?VehicleType
    {
        return $this->type;
    }

    public function setType(?VehicleType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMakeCode(): ?Make
    {
        return $this->make_code;
    }

    public function setMakeCode(?Make $make_code): self
    {
        $this->make_code = $make_code;

        return $this;
    }
}
