<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MakeRepository")
 * @ORM\Table(
 *  name="make",
 *  indexes={@ORM\Index(name="make_idx", columns={"code", "description", "type"})},
 *  uniqueConstraints={@ORM\UniqueConstraint(name="make_unique",columns={"code", "description", "type"})},
 * )
 */
class Make
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=10)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $description;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\VehicleType", inversedBy="makes")
     * @ORM\JoinColumn(name="type", referencedColumnName="code")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Model", mappedBy="make_code")
     */
    private $models;

    public function __construct()
    {
        $this->models = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getType(): ?VehicleType
    {
        return $this->type;
    }

    public function setType(?VehicleType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Model[]
     */
    public function getModels(): Collection
    {
        return $this->models;
    }

    public function addModel(Model $model): self
    {
        if (!$this->models->contains($model)) {
            $this->models[] = $model;
            $model->setMakeCode($this);
        }

        return $this;
    }

    public function removeModel(Model $model): self
    {
        if ($this->models->contains($model)) {
            $this->models->removeElement($model);
            // set the owning side to null (unless already changed)
            if ($model->getMakeCode() === $this) {
                $model->setMakeCode(null);
            }
        }

        return $this;
    }
}
