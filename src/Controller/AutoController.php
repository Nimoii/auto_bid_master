<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\VehicleType;
use App\Entity\Make;
use App\Entity\Model;

class AutoController extends AbstractController
{
    /*
     * You will need to display html page an show list of vehicle types alphabetically.
     * This list should have a link to makes route (#5).
     */
    public function index()
    {
        $types = $this->getDoctrine()->getRepository(VehicleType::class)->findAll();
        return $this->render('auto/index.html.twig', [
            'title' => 'Auto Bid Master Demo',
            'types' => $types
        ]);
    }

    /*
     * You need to display html page with dropdown that will show list of makes for selected vehicle type.
     * If make selected from this dropdown you will need send ajax request to load json data from model route (# 6) 
     * and display list of model underneath the makes dropdown. Or display message if no models available.
     */
    public function makes($type)
    {
        $makes = $this->getDoctrine()->getRepository(Make::class)->findBy(['type' => $type]);
        return $this->render('auto/makes.html.twig', [
            'title' => 'Makes - '.$type,
            'makes' => $makes,
            'type' => $type
        ]);
    }

    /*
     * To return json list of models for specific make and vehicle type.
     * Log all request for models route to database (SearchLog entity).
     */
    public function models($type, $makeCode)
    {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
                SELECT *
                FROM model
                WHERE model.type = ?
                AND model.make_code = ?
            ");
        $statement->bindValue(1, $type);
        $statement->bindValue(2, $makeCode);

        $statement->execute();
        $models = $statement->fetchAll();
        return jsonResponse::fromJsonString(json_encode($models));
    }
}
