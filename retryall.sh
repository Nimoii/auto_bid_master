#!/bin/bash

# Reset that DB
php bin/console doctrine:schema:drop --full-database --force;

# Reset migrations
rm src/Migrations/*

# Make new migration
php bin/console make:migration

# Run new migration
php bin/console doctrine:migrations:migrate --no-interaction

# Load fixtures
php bin/console doctrine:fixtures:load
