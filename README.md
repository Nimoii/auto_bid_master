# Auto Bid Master Demo

## TODO
- Parse json and format model results properly.
- Add minimum styles

### Problems
- I have a pointless fixture "AppFixtures" which I'm only using to share the loadJSON method to prevent repeating code. I would find a better way but I'd spent too long learing to set up the entities already.
- I need a better way to relate Models with Makes. Model can only map by make_code and type, but those columns are not unique..
- There are three duplicate code/types in makes.json, but those fields need to be unique in order to be set as foreign keys for Models. After struggling with this for several hours I just started started skipping those three rows.
